import React from "react";
import "./App.css";

import { Select } from "./Select";

function App() {
  return (
    <div className="App">
      <input />
      <Select selected="По имени" items={["По имени", "По фамилии"]} />
    </div>
  );
}

export default App;
