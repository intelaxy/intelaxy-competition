import React, { PureComponent } from "react";

import "./Select.css";

export class Select extends PureComponent<Props, State> {
  state = { isExpanded: false };

  handleHeadClick = (event: any) => {
    event.stopPropagation();
    this.setState(() => ({ isExpanded: !this.state.isExpanded }));
  };

  render() {
    const { selected, items = [] } = this.props;
    const { isExpanded } = this.state;
    return (
      <div className="select">
        <div className="select-selected" onClick={this.handleHeadClick}>
          {selected}
        </div>
        {isExpanded ? (
          <div className="select-items">
            {items.map(i => (
              <div>{i}</div>
            ))}
          </div>
        ) : null}
      </div>
    );
  }
}
